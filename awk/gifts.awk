#!/bin/awk -f

#awk -F'$' '{sum += $2} END {print sum}' gifts

BEGIN {
   total=250
   sum=0;
   FS="*";
   printf "%-20s %-20s \n", "Gift", "Cost"
}
{
   if ($1 == "+++") { exit } 
   if (NF != 0){ 
      split($0,g,"|")
      printf "%-20s %-20s \n", g[2], g[3] 

      split($0,c,"$")
      sum += c[2]
   }
}
END {
   remains=total-sum
   printf "\n%-20s %-20s", "Total: ", "$"sum
   printf "\n%-20s %-20s", "Remains: ", "$"remains
}
